coronavirus web scraper with python voice assistant

Voice commands availble 
-"Total number of deaths"(queries the api and recieves JSON data which is the filtered)
-"Total number of cases" (queries the api and recieves JSON data which is the filtered)
-"number of deaths in" +"[Country]"(queries the api and recieves JSON data which is the filtered)
-"number of case in" +"[Country]" (queries the api and recieves JSON data which is the filtered)
-"update" (updates the data by starting a thread and polling an endpoint)
-"Stop" (stops the script)

software
-ParseHub for webscrapping

Libaries needed:
-pyttsx3 (A text-to-speech conversion library in Python)
-SpeechRecognition (Speech recognition module for Python, supporting several engines and APIs)
