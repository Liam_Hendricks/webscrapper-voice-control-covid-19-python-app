import requests
import json
import pyttsx3
import speech_recognition as sr
import re
import threading
import time
#Author: Liam Hendricks
#Original Idea from Tech with Tim Python Project -Coronavirus Web Scaper & Voice Assistant Tutorial
#Using a different website for webscrapping
#added expections for null data
#created a totaling method for total case/deaths
API_KEY= "tzsotfyAx3OE"
PROJECT_TOKEN="tJTWoLBruCTW"
RUN_TOKEN="twfaSv0n-A57"




class Data:
    def __init__(self,api_key,project_token):
        self.api_key=api_key
        self.project_token=project_token
        self.params ={"api_key":self.api_key}
        self.data =self.get_data()
    def get_data(self):
        response =requests.get(f'https://www.parsehub.com/api/v2/projects/{self.project_token}/last_ready_run/data',params={"api_key":self.api_key})
        data =json.loads(response.text)
        return data

    def get_country_data(self,country):
        data=self.data['country']
        for content in data:
            if content['name'].lower()==country.lower():
                return content
        return "0"

    def get_total_cases(self):
        total_cases = 0
        for content in  self.data['country']:
            if content['confirmed_cases'].lower()=='confirmed cases':
                continue
            else:
                try:
                    convert =str(content['confirmed_cases']).replace(',','')
                    total_cases+=int(convert)
                except Exception as e:
                    print("Exception in confirmed cases:",str(e))   
        return str(total_cases)

    def get_total_deaths(self):
        total_deaths = 0
        for content in  self.data['country']:
            if content['confirmed_deaths']=='' or content['confirmed_deaths']=='Confirmed Deaths':
                continue
            else:
                try:
                    convert =str(content['confirmed_deaths']).replace(',','')
                    total_deaths+=int(convert)
                except Exception as e:
                    print("Exception in total deaths:",str(e))
           

        return str(total_deaths)

    # def get_total_recoveries(self):
    #     total_recoveries = 0
    #     for content in  self.data['country']:
    #         if content['confirmed_recoveries'].lower()=='' or content['confirmed_recoveries'].lower()=='confirmed recoveries':
    #             continue
    #         else:
    #             try:
    #                 convert =str(content['confirmed_recoveries']).replace(',','')
    #                 total_recoveries+=int(convert)
    #             except Exception as e:
    #                 print("Exception in recoveries:",str(e))   
    #     return str(total_recoveries)

        
    def get_list_of_countries(self):
        countries=[]
        for country in self.data['country']:
            if country['name'].lower()=='country':
                continue
            else:
                countries.append(country['name'].lower())
        return countries
    def update_data(self):
        response =requests.post(f'https://www.parsehub.com/api/v2/projects/{self.project_token}/run',params=self.params)
        
        def poll():
            time.sleep(0.1)
            old_data = self.data
            while True:
                new_data =self.get_data()
                if new_data!=old_data:
                    self.data=new_data
                    print("data updated")
                    break
                time.sleep(5)

        t =threading.Thread(target=poll)
        t.start()



def speak(text):
    engine =pyttsx3.init()
    engine.say(text)
    engine.runAndWait()

def get_audio():
    r =sr.Recognizer()
    with sr.Microphone() as source:
        audio =r.listen(source)
        said=""
        try:
            said=r.recognize_google(audio)
            print(said.lower())
        except Exception as e:
            print("Exception:",str(e))
    return said.lower()





def main():

    print("started program")
    data=Data(API_KEY,PROJECT_TOKEN)
    #print(data.data)
    #print(f"total cases: {data.get_total_cases()}")
    #print(f"total deaths: {data.get_total_deaths()}")
    #print(f"total recoveries: {data.get_total_recoveries()}")
    END_PHRASE="stop"
    country_list =data.get_list_of_countries()
    TOTAL_PATTERNS ={
        re.compile("[\w\s]+ total [\w\s]+ cases"):data.get_total_cases,
        re.compile("[\w\s]+ total cases"):data.get_total_cases,
        re.compile("[\w\s]+ total [\w\s]+ deaths"):data.get_total_deaths,
        re.compile("[\w\s]+ total deaths"):data.get_total_deaths,
        #re.compile("[\w\s]+ total [\w\s]+ recoveries"):data.get_total_recoveries,
        #re.compile("[\w\s]+ total recoveries"):data.get_total_recoveries
    }

    COUNTRY_PATTERNS ={
        re.compile("[\w\s]+ cases [\w\s]+"):lambda country: data.get_country_data(country)['confirmed_cases'],
        re.compile("[\w\s]+ deaths [\w\s]+"):lambda country: data.get_country_data(country)['confirmed_deaths'],

    }
    UPDATE_COMMAND="update"


    while True:
        print("listening ...")
        text=get_audio()
        result =None
        for pattern,func in COUNTRY_PATTERNS.items():
            if pattern.match(text):
                words =set(text.split(" "))
                for country in country_list:
                    if country in words:
                        result =func(country)
                        break
                
        for pattern,func in TOTAL_PATTERNS.items():
            if pattern.match(text):
                result = func()
                break
        if text == UPDATE_COMMAND:
            result="Data is being updated  this may take a moment!"
            data.update_data()
        if result:
            speak(result)

        if text.find(END_PHRASE)!=-1:#stop loop
            print("Ending Program")
            break
main()

        
